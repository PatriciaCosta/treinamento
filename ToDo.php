<html>
  <head>
    <link rel="stylesheet" href="todo.css">
    <meta charset="UTF-8">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#ajax_form').submit(function(){
			var dados = jQuery( this ).serialize();

			jQuery.ajax({
				type: "POST",
				url: "todo.php",
				data: dados,
				success: function( data )
				{
					alert( data );
				}
			});

			return false;
		});
	});
	</script>
  </head>

  <body>
    <div id="title">
      <p>To Do List</p>
    </div>
    <div id="form">
      <form action="todo.php" method="post">
        <input type="text" name="task" placeholder="Add a task"/>
        <input type="submit" name="submit" value="Add"/>
      </form>
    </div>
    <div id="list" style="float: left">
      <ul>
        <li>
          <?php

            if(!isset($_POST["task"])) {
              echo "No tasks yet";
              return;
            }

            $task = $_POST["task"];

            if(isset($_POST["task"])) {
              echo $task;
            }

          ?>

          <form action="todo.php" method="post">
            <input type="submit" name="complete" value="Complete!" />
          </form>

          <?php
            if(isset($_POST["complete"])){
              echo "Task Complete!";
              return;
            }

          ?>
        </li>
      </ul>
    </div>




  </body>
</html>
